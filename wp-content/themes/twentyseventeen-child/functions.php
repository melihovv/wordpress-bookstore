<?php

add_action('wp_enqueue_scripts', 'enqueue_parent_styles');

function enqueue_parent_styles()
{
    wp_enqueue_style(
        'parent-style',
        get_template_directory_uri() . '/style.css'
    );

    wp_enqueue_style(
        'grid12',
        get_stylesheet_directory_uri() . '/grid12.css'
    );
}

add_filter('bop_nav_search_show_submit_button', function ($bool, $item, $depth, $args) {
    return false;
}, 10, 4);
